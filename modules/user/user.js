const users = require("../../data/users.json");

exports.list = (req, res) => {
  res.status(200).json(users);
};

exports.userLogin = (req, res) => {
  const { username, password } = req.body;

  const user = users.find((user) => {
    console.log("di dalam find", user);
    return user.username === username;
  });
  if (user) {
    const passwordMatched = user.password === password;
    if (passwordMatched) {
      res.send(user);
    } else {
      res.status(401).send("PASSWORD SALAH");
    }
  } else {
    res.status(404).send("USER TIDAK DITEMUKAN");
  }
};

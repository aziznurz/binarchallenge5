const express = require("express");
const modulesSite = require("./modules/site/index");
const modulesUser = require("./modules/user/user");
const app = express();
const bodyParser = require("body-parser");
const port = 3000;

app.set("view engine", "ejs");

app.use(express.static("assets"));
app.use(bodyParser.json());

app.post("/login", modulesUser.userLogin);

app.get("/", modulesSite.home);

app.get("/games", modulesSite.games);

app.get("/user", modulesUser.list);

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
